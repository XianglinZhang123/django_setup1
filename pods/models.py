from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.

class Project(models.Model):
    name = models.CharField(max_length=100)
    analysis = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    client = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name