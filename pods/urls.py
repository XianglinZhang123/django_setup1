from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='pods-home'),
    path('dashboard/', views.dashboard, name='pods-dashboard'),
]