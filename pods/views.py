from django.shortcuts import render
from .models import Project

projects = [ {
	'name': 'Software1',
	'analysis':'Analysis Not Available',
	'client': 'Jane',
	'date_posted': 'August 27, 2018'
},
{
	'name': 'Software2',
	'client': 'Tim',
	'analysis':'Analysis Not Available',
	'date_posted': 'August 30, 2019'
}
]

pods = [ {
	'name': 'Review',
	'result': 'Automate looking at a design file of screenshots and scoring them based on given parameters'
},
{
	'name': 'Benchmarking',
	'result': 'More comparisons...'
},
{
	'name': 'Experiments',
	'result': 'Design visioning'
}
]

def home(request):
    context = {
        'pods': pods
    }
    return render(request, 'pods/home.html', context=context)

def dashboard(request):
	context = {
		'projects': projects
		# 'projects': Project.objects.all()
	}
	return render(request, 'pods/dashboard.html', context=context)